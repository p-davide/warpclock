package com.antonok.warpclock

import org.junit.Assert.*
import org.junit.Test

class AlarmIntentServiceKtTest {
    @Test
    fun testDescription() {
        assertEquals("1 minute", AlarmTime(0, 1).description)
        assertEquals("2 minutes", AlarmTime(0, 2).description)
        assertEquals("1 hour", AlarmTime(1, 0).description)
        assertEquals("1 hour and 1 minute", AlarmTime(1, 1).description)
        assertEquals("1 hour and 2 minutes", AlarmTime(1, 2).description)
        assertEquals("2 hours", AlarmTime(2, 0).description)
        assertEquals("2 hours and 1 minute", AlarmTime(2, 1).description)
        assertEquals("2 hours and 2 minutes", AlarmTime(2, 2).description)
        assertEquals("10 hours", AlarmTime(10, 0).description)
        assertEquals("10 hours and 1 minute", AlarmTime(10, 1).description)
        assertEquals("10 hours and 2 minutes", AlarmTime(10, 2).description)
    }
}