package com.antonok.warpclock

import android.app.TimePickerDialog
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.format.DateFormat
import android.text.style.ForegroundColorSpan
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.Button
import android.widget.TextView
import android.widget.TimePicker
import androidx.core.content.ContextCompat
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import androidx.fragment.app.FragmentActivity
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import java.util.*

const val WARP_AMOUNT_MIN = 1
const val WARP_AMOUNT_MAX = 24 * 60

const val HOUR_INCREMENT = 60
const val LARGE_MINUTE_INCREMENT = 10

const val DATASTORE_FILE = "warpclock_settings"
val Context.settingsStore: DataStore<WarpclockSettings> by dataStore(DATASTORE_FILE, serializer = WarpclockSettingsSerializer)

class MainActivity : FragmentActivity(), TimePickerDialog.OnTimeSetListener {
    private lateinit var hoursTextView: TextView
    private lateinit var incrementHoursButton: Button
    private lateinit var decrementHoursButton: Button

    private lateinit var minutesTextView: TextView
    private lateinit var increment10MinutesButton: Button
    private lateinit var increment1MinuteButton: Button
    private lateinit var decrement10MinutesButton: Button
    private lateinit var decrement1MinuteButton: Button

    // https://stackoverflow.com/questions/4284224/android-hold-button-to-repeat-action/12795551#12795551
    /**
     * A class, that can be used as a TouchListener on any view (e.g. a Button).
     * It cyclically runs a clickListener, emulating keyboard-like behaviour. First
     * click is fired immediately, next one after the initialInterval, and subsequent
     * ones after the normalInterval.
     *
     *
     * Interval is scheduled after the onClick completes, so it has to run fast.
     * If it runs slow, it does not generate skipped onClicks. Can be rewritten to
     * achieve this.
     */
    class RepeatListener(
        private val initialInterval: Long,
        private val normalInterval: Long,
        private val clickListener: View.OnClickListener
    ) : OnTouchListener
    {
        private val handler: Handler = Handler()
        private var touchedView: View? = null
        private val handlerRunnable: Runnable = object : Runnable {
            override fun run() {
                if (touchedView?.isEnabled == true) {
                    handler.postDelayed(this, normalInterval)
                    clickListener.onClick(touchedView)
                } else {
                    // if the view was disabled by the clickListener, remove the callback
                    handler.removeCallbacks(this)
                    touchedView?.isPressed = false
                    touchedView = null
                }
            }
        }

        /**
         * @param initialInterval The interval after first click event
         * @param normalInterval The interval after second and subsequent click
         * events
         * @param clickListener The OnClickListener, that will be called
         * periodically
         */
        init {
            require(initialInterval >= 0) { "negative initial interval" }
            require(normalInterval > 0) { "interval between repeats not positive" }
        }

        override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    handler.removeCallbacks(handlerRunnable)
                    handler.postDelayed(handlerRunnable, initialInterval)
                    touchedView = view
                    touchedView!!.isPressed = true
                    view.performClick()
                    return true
                }
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                    handler.removeCallbacks(handlerRunnable)
                    touchedView!!.isPressed = false
                    touchedView = null
                    return true
                }
            }
            return false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        hoursTextView = findViewById<TextView>(R.id.textViewHours);
        incrementHoursButton = findViewById<Button>(R.id.incrementHoursButton).apply {
            setOnTouchListener(RepeatListener(400, 100) { incrementHours(it) })
        }
        decrementHoursButton = findViewById<Button>(R.id.decrementHoursButton).apply {
            setOnTouchListener(RepeatListener(400, 100) { decrementHours(it) })
        }

        minutesTextView = findViewById<TextView>(R.id.textViewMinutes);
        increment10MinutesButton = findViewById<Button>(R.id.increment10MinutesButton).apply {
            setOnTouchListener(RepeatListener(400, 100) { increment10Minutes(it) })
        }
        increment1MinuteButton = findViewById<Button>(R.id.increment1MinuteButton).apply {
            setOnTouchListener(RepeatListener(400, 100) { increment1Minute(it) })
        }
        decrement10MinutesButton = findViewById<Button>(R.id.decrement10MinutesButton).apply {
            setOnTouchListener(RepeatListener(400, 100) { decrement10Minutes(it) })
        }
        decrement1MinuteButton = findViewById<Button>(R.id.decrement1MinuteButton).apply {
            setOnTouchListener(RepeatListener(400, 100) { decrement1Minute(it) })
        }

        val warpclockSettings = runBlocking { settingsStore.data.first() }
        updateViews(AlarmTime.fromMinutes(warpclockSettings.warpAmount))
    }

    fun setAlarm(@Suppress("UNUSED_PARAMETER") view: View) {
        val mIntent = Intent(this, AlarmIntentService::class.java).apply {
            action = ACTION_SET_WARP_ALARM
        }
        AlarmIntentService().enqueueWork(this, mIntent)
    }

    fun incrementHours(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(HOUR_INCREMENT) }
    }

    fun decrementHours(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(-HOUR_INCREMENT) }
    }

    fun increment10Minutes(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(LARGE_MINUTE_INCREMENT) }
    }

    fun increment1Minute(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(1) }
    }

    fun decrement10Minutes(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(-LARGE_MINUTE_INCREMENT) }
    }

    fun decrement1Minute(@Suppress("UNUSED_PARAMETER") view: View) {
        runBlocking { modifyWarpAmount(-1) }
    }

    private suspend fun modifyWarpAmount(offset: Int) {
        var newWarpAmount = 0
        settingsStore.updateData { d ->
            newWarpAmount = (d.warpAmount + offset).coerceAtLeast(WARP_AMOUNT_MIN).coerceAtMost(WARP_AMOUNT_MAX)
            d.toBuilder().setWarpAmount(newWarpAmount).build()
        }
        updateViews(AlarmTime.fromMinutes(newWarpAmount))
    }

    private fun updateViews(newTime: AlarmTime) {
        val hours = newTime.hours
        val minutes = newTime.minutes

        val faintSpan = ForegroundColorSpan(ContextCompat.getColor(application, R.color.faintText))
        val ssbHours = SpannableStringBuilder("%d".format(hours))
        ssbHours.append("h").setSpan(faintSpan, ssbHours.length - "h".length, ssbHours.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        val ssbMinutes = SpannableStringBuilder("%d".format(minutes))
        ssbMinutes.append("m").setSpan(faintSpan, ssbMinutes.length - "m".length, ssbMinutes.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        hoursTextView.text = ssbHours
        minutesTextView.text = ssbMinutes

        val canIncrement = !newTime.isAboveMax
        incrementHoursButton.isEnabled = canIncrement
        increment10MinutesButton.isEnabled = canIncrement
        increment1MinuteButton.isEnabled = canIncrement

        val canDecrement = !newTime.isBelowMin
        decrementHoursButton.isEnabled = canDecrement
        decrement10MinutesButton.isEnabled = canDecrement
        decrement1MinuteButton.isEnabled = canDecrement

        val widgetIds = AppWidgetManager.getInstance(application).getAppWidgetIds(ComponentName(application, WarpclockWidget::class.java))
        val intent = Intent(this, WarpclockWidget::class.java).apply {
            action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds)
        }
        sendBroadcast(intent)
    }

    fun setFixedAlarm(@Suppress("UNUSED_PARAMETER") view: View) {
        val (hours, minutes) = AlarmTime.now()
        TimePickerDialog(this, this, hours, minutes, DateFormat.is24HourFormat(this)).show()
    }

    override fun onTimeSet(picker: TimePicker?, hours: Int, minutes: Int) {
        val mIntent = Intent(this, AlarmIntentService::class.java).apply {
            action = ACTION_SET_FIXED_ALARM
            putExtra("hours", hours)
            putExtra("minutes", minutes)
        }
        AlarmIntentService().enqueueWork(this, mIntent)
    }
}