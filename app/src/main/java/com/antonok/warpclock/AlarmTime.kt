package com.antonok.warpclock

import java.util.Calendar

data class AlarmTime(val hours: Int, val minutes: Int) {
    val isBelowMin = hours < 0
    val isAboveMax = hours > 23
    val isValid = !isAboveMax && !isBelowMin && minutes in (0..59)
    fun toMinutes(): Int = hours * 60 + minutes
    override fun toString(): String =
        if(minutes < 10) {
            "$hours:0$minutes"
        } else {
            "$hours:$minutes"
        }
    val description: String = StringBuilder().apply {
        if (hours > 0) {
            append("$hours hour")
            if (hours != 1) {
                append("s")
            }
        }
        if (hours > 0 && minutes > 0) {
            append(" and ")
        }
        if (minutes > 0) {
            append("$minutes minute")
            if (minutes != 1) {
                append("s")
            }
        }
        
    }.toString()

    operator fun minus(that: AlarmTime) = fromMinutes(this.toMinutes() - that.toMinutes())

    companion object {
        fun fromMinutes(minutes: Int) = AlarmTime(minutes / 60, minutes % 60)
        fun now(): AlarmTime {
            val currentTime = Calendar.getInstance()

            val hour = currentTime.get(Calendar.HOUR_OF_DAY)
            val minute = currentTime.get(Calendar.MINUTE)

            return AlarmTime(hour, minute)
        }
    }
}