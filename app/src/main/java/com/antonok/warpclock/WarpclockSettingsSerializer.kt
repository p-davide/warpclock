package com.antonok.warpclock

import androidx.datastore.core.CorruptionException
import androidx.datastore.core.Serializer
import com.google.protobuf.InvalidProtocolBufferException
import java.io.InputStream
import java.io.OutputStream

const val DEFAULT_WARP_MINUTES = 8 * 60 + 0

object WarpclockSettingsSerializer : Serializer<WarpclockSettings> {
    override val defaultValue: WarpclockSettings = WarpclockSettings.newBuilder()
        .setWarpAmount(DEFAULT_WARP_MINUTES)
        .build()

    override suspend fun readFrom(input: InputStream): WarpclockSettings {
        try {
            return WarpclockSettings.parseFrom(input)
        } catch (exception: InvalidProtocolBufferException) {
            throw CorruptionException("Cannot read proto.", exception)
        }
    }

    override suspend fun writeTo(
        t: WarpclockSettings,
        output: OutputStream
    ) = t.writeTo(output)
}