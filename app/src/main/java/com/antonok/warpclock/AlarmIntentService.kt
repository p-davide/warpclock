package com.antonok.warpclock

import android.content.Intent
import android.content.Context
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.os.Handler
import android.os.Looper
import android.provider.AlarmClock
import android.widget.Toast
import androidx.core.app.JobIntentService
import java.util.*
import java.util.Calendar.HOUR_OF_DAY
import java.util.Calendar.MINUTE
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

const val ACTION_SET_WARP_ALARM = "com.antonok.warpclock.action.SET_WARP_ALARM"
const val ACTION_SET_FIXED_ALARM = "com.antonok.warpclock.action.SET_FIXED_ALARM"

/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
class AlarmIntentService : JobIntentService() {
    private var mHandler: Handler = Handler(Looper.getMainLooper())

    fun enqueueWork(context: Context, intent: Intent) {
        enqueueWork(context, AlarmIntentService::class.java, 2, intent)
    }

    override fun onCreate() {
        super.onCreate()
    }

    override fun onHandleWork(intent: Intent) {
        when (intent.action) {
            ACTION_SET_WARP_ALARM -> {
                val warpAmount = warpAmountFromSettings()
                val time = warpedTime(warpAmount)
                handleActionSetAlarm(time)
                toast(time, warpAmount)
            }
            ACTION_SET_FIXED_ALARM -> {
                val time = AlarmTime(intent.getIntExtra("hours", -1), intent.getIntExtra("minutes", 0))
                handleActionSetAlarm(time)
                toast(time)
            }
        }
    }

    /**
     * Handle action SetAlarm in the provided background thread with the provided
     * parameters.
     */
    private fun handleActionSetAlarm(time: AlarmTime) {
        // Set the new alarm using an Intent
        val alarmIntent = Intent(AlarmClock.ACTION_SET_ALARM).apply {
            putExtra(AlarmClock.EXTRA_HOUR, time.hours)
            putExtra(AlarmClock.EXTRA_MINUTES, time.minutes)
            putExtra(AlarmClock.EXTRA_SKIP_UI, true)
        }
        alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(alarmIntent)

        //Update the widget
        val appWidgetManager = AppWidgetManager.getInstance(this)
        val widgetIds = appWidgetManager.getAppWidgetIds(ComponentName(this, WarpclockWidget::class.java))
        for (appWidgetId in widgetIds) {
            WarpclockWidget.updateAppWidget(applicationContext, appWidgetManager, appWidgetId)
        }
    }

    private fun warpAmountFromSettings(): Int = runBlocking { settingsStore.data.first() }.warpAmount

    private fun warpedTime(warpAmount: Int): AlarmTime {
        val (hours, minutes) = AlarmTime.now()

        val alarmMinute = (minutes + warpAmount) % 60
        val alarmHour = (hours + (minutes + warpAmount) / 60) % 24
        return AlarmTime(alarmHour, alarmMinute)
    }
    private fun toast(time: AlarmTime, warpAmount: Int = -1) {
        val diff = time - AlarmTime.now()
        val description = if (warpAmount == -1) String() else "${diff.description} "
        val toastMsg = "Warp ahead ${description}to $time"
        // Show a toast.
        mHandler.post {
            Toast.makeText(this, toastMsg, Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        /**
         * Starts this service to set an alarm with the given parameters. If
         * the service is already performing a task this action will be queued.
         *
         * @see IntentService
         */
        @JvmStatic
        fun startSetAlarm(context: Context) {
            val intent = Intent(context, AlarmIntentService::class.java).apply {
                action = ACTION_SET_WARP_ALARM
            }
            context.startService(intent)
        }
    }
}