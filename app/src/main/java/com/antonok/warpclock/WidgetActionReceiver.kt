package com.antonok.warpclock

import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context

class WidgetActionReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        intent.setClass(context, AlarmIntentService::class.java)
        AlarmIntentService().enqueueWork(context, intent)
    }
}
